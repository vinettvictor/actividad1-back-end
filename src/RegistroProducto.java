package com.everis.actividad;

import java.util.ArrayList;

public class RegistroProducto {
	
	ArrayList<Producto> productos;
	
	public RegistroProducto() {
		super();
		this.productos = new ArrayList<Producto>();
	}

	//M�todos
	public void agregarProducto(Producto producto) {
		this.productos.add(producto);
	}
	
	public void listarProducto() {
		for(int i =0; i< this.productos.size(); i++) {
			Producto producto = this.productos.get(i);
			if(producto.getPrecioBase() > 10000) {
				System.out.println("Producto n�"+i+ ":"+producto.getNombre());
			}			
		}
	}
	
	public void eliminarProducto(Producto producto) {
		if(producto.getPrecioBase() < 2000) {
			this.productos.remove(producto);
			System.out.println("Se elimino el siguiente producto: "+producto.getNombre());
		}
	}
}
