package com.everis.actividad;

public class Main {

	public static void main(String[] args) {
		
		Producto producto1 = new Producto("1",100000,"paracetamol",false,false,2,"vitaminas buenas","información del producto1");
		Producto producto2 = new Producto("2",1500,"ibuprofeno",true,false,2,"vitaminas buenas","información del producto2");
		Producto producto3 = new Producto("3",1000,"creatina",false,true,2,"vitaminas buenas","información del producto3");
		Producto producto4 = new Producto("4",12000,"whey protein",false,true,2,"vitaminas buenas","información del producto3");
		
		RegistroProducto registro = new RegistroProducto();
		registro.agregarProducto(producto1);
		registro.agregarProducto(producto2);
		registro.agregarProducto(producto3);
		registro.agregarProducto(producto4);
		
		System.out.println("");
		System.out.println("Lista de productos");
		registro.listarProducto();
		
		System.out.println("");
		System.out.println("Caracteristicas de los productos");
		System.out.println(producto1.toString());
		System.out.println(producto2.toString());
		System.out.println(producto3.toString());
		System.out.println(producto4.toString());
		
		System.out.println("");
		System.out.println("Descuentos para el producto 1");
		producto1.descuento("lunes");
		
		System.out.println("");
		System.out.println("Se eliminaron los siguientes productos");
		registro.eliminarProducto(producto3);
		registro.eliminarProducto(producto4);
		

		producto1.recargo();

	}

}
