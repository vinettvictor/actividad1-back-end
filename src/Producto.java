package com.everis.actividad;

public class Producto {
	/*
	 * c�digo, precioBase (mayor a 0), nombre (largo m�nimo 3 caracteres), 
	 * gen�rico (si el medicamento es gen�rico o no), 
	 * cantidadVitaminas, informacionVitaminas 
	 * y contraindicaciones.
	 */
	
	private String codigo;
	private double precioBase;
	private String nombre;
	private boolean generico;
	private boolean suplemento;
	private int cantidadVit;
	private String informacionVit;
	private String contraIndicaciones;
	
	public Producto(String codigo, double precioBase, String nombre ,boolean generico,boolean suplemento, int cantidadVit,
			String informacionVit, String contraIndicaciones) {
		super();
		this.codigo = codigo;
		this.precioBase = precioBase;
		this.nombre = nombre;
		this.generico = generico;
		this.suplemento = suplemento;
		this.cantidadVit = cantidadVit;
		this.informacionVit = informacionVit;
		this.contraIndicaciones = contraIndicaciones;
	}
	
	

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public double getPrecioBase() {
		return precioBase;
	}

	public void setPrecioBase(double precioBase) {
		this.precioBase = precioBase;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public boolean isGenerico() {
		return generico;
	}

	public void setGenerico(boolean generico) {
		this.generico = generico;
	}

	public int getCantidadVit() {
		return cantidadVit;
	}

	public void setCantidadVit(int cantidadVit) {
		this.cantidadVit = cantidadVit;
	}

	public String getInformacionVit() {
		return informacionVit;
	}

	public void setInformacionVit(String informacionVit) {
		this.informacionVit = informacionVit;
	}

	public String getContraIndicaciones() {
		return contraIndicaciones;
	}

	public void setContraIndicaciones(String contraIndicaciones) {
		this.contraIndicaciones = contraIndicaciones;
	}
	
	public boolean isSuplemento() {
		return suplemento;
	}

	public void setSuplemento(boolean suplemento) {
		this.suplemento = suplemento;
	}

	//m�todo para mostrar caracteristicas del producto
	@Override
	public String toString() {
		return "Producto [codigo=" + codigo + ", precioBase=" + precioBase + ", nombre=" + nombre + ", generico="
				+ generico + ", suplemento=" + suplemento + ", cantidadVit=" + cantidadVit + ", informacionVit="
				+ informacionVit + ", contraIndicaciones=" + contraIndicaciones + "]";
	}	
	
	//metodo para los descuentos
	public void descuento(String dia) {	
		if(dia.equals("lunes")) {
			if(this.generico == true) {
				this.precioBase = this.precioBase - (this.precioBase*0.1);
				System.out.println("se aplico descuento del 10%");
			}
							
			if(this.suplemento == true) {
				this.precioBase = this.precioBase - (this.precioBase*0.1);
				System.out.println("Descuento Aplicado del 10%");
			}else {
				
			}
		}		
	}
	
	
	public void recargo() {
		if(!this.generico) {
			this.precioBase = this.precioBase + this.precioBase*0.2;
			System.out.println("se aplico un recargo del 20% al precio base");
		}
		
		if(this.suplemento) {
			this.precioBase = this.precioBase + this.precioBase*(this.cantidadVit*0.02);
		}
	}
}
